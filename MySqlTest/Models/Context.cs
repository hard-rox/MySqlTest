﻿using System.Data.Entity;

[DbConfigurationType(typeof(MySql.Data.Entity.MySqlEFConfiguration))]
class MySqlDbContext : DbContext
{
    public MySqlDbContext() : base("testMySql") { }

    public DbSet<TestModel> Models { get; set; }
}