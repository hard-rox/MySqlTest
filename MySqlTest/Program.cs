﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MySqlTest
{
    class Program
    {
        static void Main(string[] args)
        {
            MySqlDbContext context = new MySqlDbContext();
            if (context.Database.Exists())
                Console.WriteLine("DB connected.");
            else
                Console.WriteLine("Not Connected.");

            context.Models.Add(new TestModel() { Name = "Roxy" });
            var result = context.SaveChanges();

            Console.WriteLine(result);
            Console.ReadKey();
        }
    }
}
